package re.obsqu.bfeed;

public class CalculationData {
    private long hours;
    private long minutes;

    public CalculationData(long hours, long minutes) {
        this.hours = hours;
        this.minutes = minutes;
    }
}
