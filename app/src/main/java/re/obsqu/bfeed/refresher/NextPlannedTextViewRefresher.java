package re.obsqu.bfeed.refresher;

import android.content.Context;
import android.widget.TextView;
import re.obsqu.bfeed.MainActivity;
import re.obsqu.bfeed.R;
import re.obsqu.bfeed.UiRefresher;

public class NextPlannedTextViewRefresher implements UiRefresher {

    private Context context;

    public NextPlannedTextViewRefresher(Context context) {
        this.context = context;
    }

    @Override
    public void refreshWith(String value) {
        final TextView feedNextPlanned = (TextView) ((MainActivity) context).findViewById(R.id.feed_next_planned);
        feedNextPlanned.setText(value);
    }
}
