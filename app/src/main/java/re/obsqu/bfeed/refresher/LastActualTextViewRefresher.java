package re.obsqu.bfeed.refresher;

import android.content.Context;
import android.widget.TextView;
import re.obsqu.bfeed.MainActivity;
import re.obsqu.bfeed.R;
import re.obsqu.bfeed.UiRefresher;

public class LastActualTextViewRefresher implements UiRefresher {

    private Context context;

    public LastActualTextViewRefresher(Context context) {
        this.context = context;
    }

    @Override
    public void refreshWith(String value) {
        final TextView feedLastActual = (TextView) ((MainActivity) context).findViewById(R.id.feed_last_actual);
        feedLastActual.setText(value);
    }
}
