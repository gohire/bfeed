package re.obsqu.bfeed.refresher;

import android.content.Context;
import android.widget.TextView;
import re.obsqu.bfeed.MainActivity;
import re.obsqu.bfeed.R;
import re.obsqu.bfeed.UiRefresher;

public class NextPlannedCureTextViewRefresher implements UiRefresher {

    private Context context;

    public NextPlannedCureTextViewRefresher(Context context) {
        this.context = context;
    }

    @Override
    public void refreshWith(String value) {
        final TextView cureNextPlanned = (TextView) ((MainActivity) context).findViewById(R.id.cure_next_planned);
        cureNextPlanned.setText(value);
    }
}
