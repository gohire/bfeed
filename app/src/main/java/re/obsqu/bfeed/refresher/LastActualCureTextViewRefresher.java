package re.obsqu.bfeed.refresher;

import android.content.Context;
import android.widget.TextView;
import re.obsqu.bfeed.MainActivity;
import re.obsqu.bfeed.R;
import re.obsqu.bfeed.UiRefresher;

public class LastActualCureTextViewRefresher implements UiRefresher {

    private Context context;

    public LastActualCureTextViewRefresher(Context context) {
        this.context = context;
    }

    @Override
    public void refreshWith(String value) {
        final TextView cureLastActual = (TextView) ((MainActivity) context).findViewById(R.id.cure_last_actual);
        cureLastActual.setText(value);
    }
}
