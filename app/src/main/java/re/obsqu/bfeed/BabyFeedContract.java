package re.obsqu.bfeed;

import android.provider.BaseColumns;

import static re.obsqu.bfeed.BabyFeedContract.BabyFeedEntry.COLUMN_NAME_FEED_TYPE;
import static re.obsqu.bfeed.BabyFeedContract.BabyFeedEntry.TABLE_NAME;

public class BabyFeedContract    {

    public enum TYPE   {
        FEED(1), CURE(2), SLEEP(3);

        private int value;

        TYPE(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    public BabyFeedContract() {}

    public static abstract class BabyFeedEntry implements BaseColumns {
        public static final String TABLE_NAME = "feed";
        public static final String COLUMN_NAME_FEED_ID = "fid";
        public static final String COLUMN_NAME_DATE_START = "fstart";
        public static final String COLUMN_NAME_DATE_END = "fend";
        public static final String COLUMN_NAME_FEED_TYPE = "ftype";
        public static final String COLUMN_NAME_NULLABLE = "hack";
    }

    public static final String TEXT_TYPE = " TEXT";
    public static final String COMMA_SEP = ",";
    public static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE IF NOT EXISTS" + TABLE_NAME + " (" +
                    BabyFeedEntry._ID + " INTEGER PRIMARY KEY," +
                    BabyFeedEntry.COLUMN_NAME_FEED_ID + TEXT_TYPE + COMMA_SEP +
                    BabyFeedEntry.COLUMN_NAME_DATE_START + TEXT_TYPE + COMMA_SEP +
                    BabyFeedEntry.COLUMN_NAME_DATE_END + TEXT_TYPE +
            " )";

    public static final String SQL_ADD_COLUMN = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + COLUMN_NAME_FEED_TYPE +" INTEGER;";
    public static final String SQL_FILL_COLUMN = "UPDATE " + TABLE_NAME + " SET " + COLUMN_NAME_FEED_TYPE + " = 1";

    public static final String SQL_GET_LAST_ROW = "SELECT * FROM " + TABLE_NAME + " ORDER BY "
            + BabyFeedEntry._ID + " DESC LIMIT 1";

    public static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + TABLE_NAME;



}
