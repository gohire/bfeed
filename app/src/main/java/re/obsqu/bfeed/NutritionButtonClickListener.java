package re.obsqu.bfeed;

import android.view.View;
import io.reactivex.functions.Consumer;

import java.util.List;

public class NutritionButtonClickListener<T extends BabyFeedContract.TYPE> implements View.OnClickListener, Consumer {
    
    private T type;

    private BabyFeedDbHelper dbHelper;
    private List<UiRefresher> uiRefreshers;
    private CalculationThread calculationThread;

    public NutritionButtonClickListener(T type, BabyFeedDbHelper dbHelper, List<UiRefresher> uiRefreshers, CalculationThread calculationThread) {
        this.type = type;
        this.dbHelper = dbHelper;
        this.uiRefreshers = uiRefreshers;
        this.calculationThread = calculationThread;
    }

    @Override
    public void onClick(View v) {
        dbHelper.createFeed(type);
        for (UiRefresher uiRefresher : uiRefreshers)    {
            uiRefresher.refreshWith(calculationThread.getLr().first.getText().toString());
        }
    }

    @Override
    public void accept(Object t) throws Exception {
        dbHelper.createFeed(type);
        for (UiRefresher uiRefresher : uiRefreshers)    {
            uiRefresher.refreshWith(calculationThread.getLr().first.getText().toString());
        }
    }
}
