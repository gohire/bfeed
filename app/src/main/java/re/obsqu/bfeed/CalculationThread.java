package re.obsqu.bfeed;

import android.content.Context;
import android.util.Pair;
import android.widget.TextView;

import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import static java.lang.Math.abs;

public class CalculationThread implements Runnable {

    private BabyFeedDbHelper dbHelper;

    private CalculationData calculationData;

    private Pair<TextView, TextView> lr;


    public CalculationThread(BabyFeedDbHelper dbHelper, Pair<TextView, TextView> lr) {
        this.dbHelper = dbHelper;
        this.lr = lr;
    }

    public Pair<TextView, TextView> getLr() {
        return lr;
    }

    @Override
    public void run() {
        Date current = Calendar.getInstance().getTime();
        String s = dbHelper.getLastFeedDate();
        Date last = s != null ? new Date(s) : new Date();

        Pair<Long, Long> hm = calculate(last, current);
        new CalculationData(hm.first, (hm.first != 0 ? hm.second % (hm.first * 60) : hm.second));
        lr.first.setText(hm.first + "h " + (hm.first != 0 ? hm.second % (hm.first * 60) : hm.second) + "m");

        Calendar cal = Calendar.getInstance();
        cal.setTime(last);
        cal.add(Calendar.HOUR_OF_DAY, 3);// TODO: hardcoded

        hm = calculate(current, cal.getTime());

        lr.second.setText(hm.first + "h " + abs(hm.first != 0 ? hm.second % (hm.first * 60) : hm.second) + "m");
    }


    private Pair<Long, Long> calculate(Date last, Date current) {
        long diff = current.getTime() - last.getTime();
        long minutes = TimeUnit.MILLISECONDS.toMinutes(diff);
        long hours = TimeUnit.MILLISECONDS.toHours(diff);
        return new Pair<>(hours, minutes);
    }
}
