package re.obsqu.bfeed;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

import static re.obsqu.bfeed.BabyFeedContract.*;
import static re.obsqu.bfeed.BabyFeedContract.BabyFeedEntry.COLUMN_NAME_FEED_TYPE;
import static re.obsqu.bfeed.BabyFeedContract.BabyFeedEntry.TABLE_NAME;

public class BabyFeedDbHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 2;
    public static final String DATABASE_NAME = "BabyFeed.db";

    public BabyFeedDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public List<String> getAppCategoryDetail() {

        String selectQuery = "SELECT  * FROM " + TABLE_NAME;
        SQLiteDatabase db  = this.getReadableDatabase();
        Cursor cursor      = db.rawQuery(selectQuery, null);
        String[] data      = null;

        List<String> list = new ArrayList<>();

        if (cursor.moveToFirst()) {
            do {
                list.add(cursor.getString(0));
                list.add(cursor.getString(1));
                list.add(cursor.getString(2));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return list;
    }

    public String getLastFeedDate() {
        SQLiteDatabase db  = this.getReadableDatabase();
        Cursor cursor      = db.rawQuery(SQL_GET_LAST_ROW, null);
        String result = null;
        if (cursor.moveToFirst()) {
            result = cursor.getString(2);
        }
        cursor.close();
        return result;
    }

    public long createFeed(TYPE feedType) {
        ContentValues values = new ContentValues();
        values.put(BabyFeedContract.BabyFeedEntry.COLUMN_NAME_FEED_ID, UUID.randomUUID().toString());
        values.put(BabyFeedContract.BabyFeedEntry.COLUMN_NAME_DATE_START, Calendar.getInstance().getTime().toString());
        values.put(BabyFeedContract.BabyFeedEntry.COLUMN_NAME_FEED_TYPE, feedType.getValue());

        final SQLiteDatabase db = getWritableDatabase();

        return db.insert(
                BabyFeedContract.BabyFeedEntry.TABLE_NAME,
                BabyFeedContract.BabyFeedEntry.COLUMN_NAME_NULLABLE,
                values);
    }

    public void clearDB() {
        final SQLiteDatabase db = getWritableDatabase();
        db.execSQL(SQL_DELETE_ENTRIES);
    }
}
