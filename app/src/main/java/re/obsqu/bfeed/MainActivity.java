package re.obsqu.bfeed;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Pair;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;
import com.jakewharton.rxbinding2.view.RxView;
import io.reactivex.functions.Consumer;
import re.obsqu.bfeed.refresher.LastActualCureTextViewRefresher;
import re.obsqu.bfeed.refresher.LastActualTextViewRefresher;
import re.obsqu.bfeed.refresher.NextPlannedCureTextViewRefresher;
import re.obsqu.bfeed.refresher.NextPlannedTextViewRefresher;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static re.obsqu.bfeed.BabyFeedContract.TYPE.CURE;
import static re.obsqu.bfeed.BabyFeedContract.TYPE.FEED;
import static re.obsqu.bfeed.BabyFeedContract.TYPE.SLEEP;

public class MainActivity extends AppCompatActivity {

//    private final TextView feedLastActual = (TextView) findViewById(R.id.feed_last_actual);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final TextView feedLastActual = (TextView) findViewById(R.id.feed_last_actual);
        final TextView feedNextPlanned = (TextView) findViewById(R.id.feed_next_planned);
        final BabyFeedDbHelper mDbHelper = new BabyFeedDbHelper(getApplicationContext());
//        mDbHelper.clearDB();

        final CalculationThread calculationThread = new CalculationThread(
                mDbHelper,
                new Pair<>(feedLastActual, feedNextPlanned)
        );

        Thread thread = new Thread()    {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        runOnUiThread(calculationThread);
                    }
                    Thread.sleep(60_000);
                }
                catch (InterruptedException ignored)    {

                }
            }
        };
        thread.start();

        List<UiRefresher> feedUiRefreshers = new ArrayList<UiRefresher>(){{
            add(new LastActualTextViewRefresher(getApplicationContext()));
            add(new NextPlannedTextViewRefresher(getApplicationContext()));
        }};

        final Button feedButton = findViewById(R.id.feed_button);
        NutritionButtonClickListener<BabyFeedContract.TYPE> feedButtonClickListener = new NutritionButtonClickListener<>(
                FEED, mDbHelper, feedUiRefreshers, calculationThread);

        RxView.clicks(feedButton).debounce(10_000, TimeUnit.MICROSECONDS).subscribe(feedButtonClickListener);
//        feedButton.setOnClickListener(feedButtonClickListener);

        List<UiRefresher> cureUiRefreshers = new ArrayList<UiRefresher>(){{
            add(new LastActualCureTextViewRefresher(getApplicationContext()));
            add(new NextPlannedCureTextViewRefresher(getApplicationContext()));
        }};

        final Button cureButton = findViewById(R.id.cure_button);
        NutritionButtonClickListener<BabyFeedContract.TYPE> cureButtonClickListener = new NutritionButtonClickListener<>(
                CURE, mDbHelper, cureUiRefreshers, calculationThread);
        RxView.clicks(cureButton).debounce(10_000, TimeUnit.MICROSECONDS).subscribe(cureButtonClickListener);
//        cureButton.setOnClickListener(cureButtonClickListener);

        List<UiRefresher> sleepUiRefreshers = new ArrayList<UiRefresher>(){{
            add(new LastActualCureTextViewRefresher(getApplicationContext()));
            add(new NextPlannedCureTextViewRefresher(getApplicationContext()));
        }};

        final Button sleepButton = findViewById(R.id.sleep_button);
        NutritionButtonClickListener<BabyFeedContract.TYPE> sleepButtonClickListener = new NutritionButtonClickListener<>(
                SLEEP, mDbHelper, cureUiRefreshers, calculationThread);
        RxView.clicks(sleepButton).debounce(10_000, TimeUnit.MICROSECONDS).subscribe(sleepButtonClickListener);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            // launch settings activity
            startActivity(new Intent(MainActivity.this, SettingsActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
