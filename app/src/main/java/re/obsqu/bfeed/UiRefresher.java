package re.obsqu.bfeed;

public interface UiRefresher {

    void refreshWith(String value);
}
